/*
 * somfy-cgi-settings.c
 *
 *  Created on: Feb 15, 2016
 *      Author: poppe
 */
#include "nuttx/config.h"

#include <sys/types.h>

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
//#include <nuttx/fs/ioctl.h>

#include <nuttx/ieee802154/ieee802154.h>
#include <nuttx/ieee802154/ieee802154_dev.h>

int regdump_binfs(int argc, char *argv[])
{
	int fd;
	int error;
	printf("Running regdump\n");
	
	fd = open("/dev/ieee0", O_RDWR);
	
	if (fd < 0)
	{
		printf("ERROR: open failed: %d\n", errno);
		return OK;
	}

	error = ioctl(fd, MAC854IORDUMP, (unsigned long)NULL);
	if (error)
	{
		printf("ERROR: dump failed\n");
		return OK;
	}

	error = close(fd);
	if (error)
	{
		printf("ERROR: close failed\n");
	}
	return OK;
}
